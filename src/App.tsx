import React, { useState } from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import './App.global.css';
import ListFiles from "./components/ListFiles"

const ResizeApp = () => {
  const [files, setFiles] = useState([]);
  const [folderSelect, setFolderSelect] = useState(false);
  const selectFolder = (folder: any) => {
    const inputFiles: File[] = Object.values(folder.target.files);

    // Filter les images
    inputFiles.filter((f) => f.type.startsWith('image'));

    setFolderSelect(true);
    setFiles(inputFiles);
  };
  return (
    <div>
      <input
        /* @ts-expect-error import folder */
        directory=""
        webkitdirectory=""
        type="file"
        onChange={selectFolder}
      />
      {folderSelect && (
        <>
          {files.length > 0 ? (
            <ListFiles files={files}/>
            // <h3>Pas de fichiers dans le dossier</h3>

          ) : (
            <h3>Pas de fichiers dans le dossier</h3>
          )}
        </>
      )}
    </div>
  );
};

export default function App() {
  return (
    <Router>
      <Switch>
        <Route path="/" component={ResizeApp} />
      </Switch>
    </Router>
  );
}

import React from 'react';

type ListFilesProps = {
    files: File[]
}
const ListFiles = ({files}: ListFilesProps) => {
  return (
    <div>
        <ul>
        {files.map(file => {
            return (
                <li>{file.name}</li>
            )
        })}
        </ul>
    </div>
  );
};



export default ListFiles;